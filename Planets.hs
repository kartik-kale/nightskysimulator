module Planets (Planets(..), planetRA, planetDec,getRaDec,getRaDecDist) where

import Data.Fixed
import qualified Sun as S
import qualified Astro as A

data Planets = Mercury | Moon | Venus | Mars | Jupiter | Saturn | Uranus | Neptune deriving (Show)

-------------------------------------------------------------
-- (Long of asc. node)
isN Mercury d = 48.3313 + 3.24587E-5 * d  
isN Venus d= 76.6799 + 2.46590E-5 * d
isN Mars d =  49.5574 + 2.11081E-5 * d
isN Jupiter d = 100.4542 + 2.76854E-5 * d
isN Saturn d = 113.6634 + 2.38980E-5 * d
isN Uranus d =  74.0005 + 1.3978E-5 * d
isN Neptune d = 131.7806 + 3.0173E-5 * d
isN Moon d = 125.1228 - 0.0529538083 * d



--(Inclination)
i Mercury d= 7.0047 + 5.00E-8 * d
i Moon _ = 5.1454
i Venus d= 3.3946 + 2.75E-8 * d
i Jupiter d = 1.3030 - 1.557E-7 * d
i Mars d = 1.8497 - 1.78E-8 * d
i Saturn d = 2.4886 - 1.081E-7 * d
i Uranus d = 0.7733 + 1.9E-8 * d
i Neptune d = 1.7700 - 2.55E-7 * d



--(Argument of perihelion)

w Mercury d= 29.1241 + 1.01444E-5 * d
w Moon d  = 318.0634 + 0.1643573223 * d
w Venus d=  54.8910 + 1.38374E-5 * d
w Jupiter d = 273.8777 + 1.64505E-5 * d
w Mars d = 286.5016 + 2.92961E-5 * d
w Saturn d = 339.3939 + 2.97661E-5 * d
w Uranus d =  96.6612 + 3.0565E-5 * d
w Neptune d = 272.8461 - 6.027E-6 * d




-- (Semi-major axis)
a Mercury _= 0.387098
a Moon _  = 60.2666
a Venus _ = 0.723330 
a Mars _= 1.523688
a Jupiter d = 5.20256
a Saturn _= 9.55475
a Uranus d = 19.18171 - 1.55E-8 * d 
a Neptune d = 30.05826 + 3.313E-8 * d

-- (Eccentricity)

e Mercury d= 0.205635 + 5.59E-10 * d   
e Venus d= 0.006773 - 1.302E-9 * d
e Mars d = 0.093405 + 2.516E-9 * d
e Jupiter d = 0.048498 + 4.469E-9 * d
e Saturn d = 0.055546 - 9.499E-9 * d
e Uranus d = 0.047318 + 7.45E-9 * d
e Neptune d = 0.008606 + 2.15E-9 * d
e Moon _  = 0.054900



isM Mercury d= mod' (168.6562 + 4.0923344368 * d) 360
isM Venus d =  48.0052 + 1.6021302244 * d
isM Neptune d = 260.2471 + 0.005995147 * d
isM Saturn d = 316.9670 + 0.0334442282 * d
isM Uranus d = 142.5905 + 0.011725806 * d
isM Mars d =  18.6021 + 0.5240207766 * d
isM Jupiter d =  19.8950 + 0.0830853001 * d
isM Moon d  = 115.3654 + 13.0649929509 * d





getEccentricAnomaly p d = isM p d + (e p d*(180/pi)) * sin (A.degToRad (isM p d)) * ( 1.0 + e p d* cos (A.degToRad (isM p d)))


xv p d =a p d*(cos(A.degToRad $ getEccentricAnomaly p d) - e p d)

yv p d = a p d*(sqrt (1.0 -e p d*e p d) * sin (A.degToRad $ getEccentricAnomaly p d)) 

v p d= A.radToDeg (atan2 (yv p d) (xv p d))

r p d= sqrt( xv p d*xv p d+ yv p d*yv p d)






xh p d= r p d * ( cos(A.degToRad (isN p d)) * cos(A.degToRad (v p d+w p d)) - sin(A.degToRad (isN p d)) * sin(A.degToRad (v p d+w p d)) * cos(A.degToRad (i p d)) )

yh p d= r p d * ( sin(A.degToRad (isN p d)) * cos(A.degToRad (v p d+w p d)) + cos(A.degToRad (isN p d)) * sin(A.degToRad (v p d+w p d)) * cos(A.degToRad (i p d)) )

zh p d= r p d * ( sin(A.degToRad (v p d+w p d)) * sin(A.degToRad (i p d)) )

lonecl p d= A.radToDeg(atan2 (yh p d) (xh p d)) 
latecl p d= A.radToDeg(atan2 (zh p d) (sqrt((xh p d*xh p d)+(yh p d*yh p d)) ))

xhc p d = r p d * cos(A.degToRad (lonecl p d)) * cos(A.degToRad (latecl p d))
yhc p d = r p d * sin(A.degToRad (lonecl p d)) * cos(A.degToRad (latecl p d))
zhc p d = r p d * sin(A.degToRad (latecl p d))



xs p d= S.r d * cos(A.degToRad (S.lonsun d))
ys p d= S.r d * sin(A.degToRad (S.lonsun d))

xg Moon d = xhc Moon d
xg p d= xhc p d+ xs p d

yg Moon d = yhc Moon d
yg p d= yhc p d+ ys p d

zg  = zhc

xe  = xg 
ye p d= yg p d * cos(A.degToRad (S.getEcl d)) - zg p d * sin(A.degToRad (S.getEcl d)) 
ze p d= yg p d * sin(A.degToRad (S.getEcl d)) + zg p d * cos(A.degToRad (S.getEcl d))

planetRA p d= A.radToDeg $ atan2 (ye p d) (xe p d)
planetDec p d = A.radToDeg $ atan2 (ze p d) (sqrt(xe p d*xe p d+ye p d*ye p d))

rg p d= sqrt(xe p d*xe p d+ye p d*ye p d+ze p d*ze p d)

getRaDec y m d p = (planetRA p days, planetDec p days)
			where
			days=fromIntegral $ A.daysFromMillenium y m d

getRaDecDist y m d p = (planetRA p days, planetDec p days,rg p days)
                     where        
                        days=fromIntegral $ A.daysFromMillenium y m d



