from bs4 import BeautifulSoup
import pycurl
from StringIO import StringIO
import csv
import re

maxMag = 7
outputFile = 'shortTableSepSingleChar.csv'
fobj = open("listOfConstells")
fout = open(outputFile,'a+')
for line in fobj:
	url = "https://en.wikipedia.org"+line.strip()
	constellation = line.strip().replace("/wiki/List_of_stars_in_","").replace('_',' ')
	print "Doing "+constellation
	buffer = StringIO()
	c = pycurl.Curl()
	c.setopt(c.URL, url)
	c.setopt(c.WRITEDATA, buffer)
	c.perform()
	c.close()
	html_doc = buffer.getvalue()
	soup = BeautifulSoup(html_doc, 'html.parser')
	data = []
	table = soup.find('table', attrs={'class':'wikitable'})
	rows = table.find_all('tr')
	magnitudeHeaderNo = -1
	raHeaderNo = -1
	nameHeaderNo = 0
	decHeaderNo = -1
	for row in rows:
		headers = row.find_all('th')
		if len(headers) > 0:
			headerNo = -1
			for header in headers:
				headerNo+=1
				headerLink = header.find('a')
				headerName = 'unknown'
				if headerName == u'/wiki/Proper_names_(astronomy)':
					nameHeaderNo = headerNo
				if headerLink:
					headerName = headerLink['href']
				if headerName == u'/wiki/Apparent_magnitude':
					magnitudeHeaderNo=headerNo
				if headerName == u'/wiki/Right_ascension':
					raHeaderNo = headerNo
				if headerName == u'/wiki/Declination':
					decHeaderNo = headerNo
			break
	print magnitudeHeaderNo
	print nameHeaderNo
	print raHeaderNo
	print decHeaderNo
	maxHeaderIndex = max(nameHeaderNo,raHeaderNo,decHeaderNo,magnitudeHeaderNo)
	for row in rows:
		cols = row.find_all('td')
		cols = [ele.text.strip() for ele in cols]
		if cols: 
			if len(cols) > maxHeaderIndex:
				apparentMag = float(re.findall("\d+.\d+",cols[magnitudeHeaderNo])[0])
				if apparentMag >maxMag:
					break
			
		data.append(cols) # Get rid of empty values
	for row in data:
		if len(row)>=maxHeaderIndex:
			validMag = False
			validRA = False
			validDec=False
			name = row[0].encode('utf-8')
			
			raValueArray = re.findall('-?\+?\d+\.?\d+',row[raHeaderNo])
			if len(raValueArray)>2:	
				ra = float(raValueArray[0])*15+float(raValueArray[1])/4+float(raValueArray[2])/240
				validRA = True
				if u'\u2212' in row[raHeaderNo]:
					ra *= (-1)
			
			decValueArray = re.findall('-?\+?\d+\.?\d+',row[decHeaderNo])
			if len(decValueArray)>2:	
				dec = float(decValueArray[0])+float(decValueArray[1])/60+float(decValueArray[2])/3600
				if u'\u2212' in row[decHeaderNo]:
					dec *= (-1)
				validDec = True
				
			if len(re.findall('-?\+?\d+\.?\d+',row[magnitudeHeaderNo]))>0:
				apparentMag = float(re.findall("\d+.\d+",row[magnitudeHeaderNo])[0])
				validMag = True
				if u'\u2212' in row[magnitudeHeaderNo]:
					apparentMag *= (-1)
			if validMag and validRA and validDec:
				fout.write(name+'@'+constellation+'@'+str(ra)+'@'+str(dec)+'@'+str(apparentMag)+'\n')
fout.close()

