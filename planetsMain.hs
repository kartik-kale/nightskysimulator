import System.IO
import System.Environment
import Data.Char
import Planets
import Astro
import Sun
data PlanetWithPos = PlanetWithPos { planet::Planets, ra::Double, dec::Double} deriving (Show)

toPlanetWithPos::Integer->Int->Int->Planets->PlanetWithPos
toPlanetWithPos y m d p = (PlanetWithPos {planet = p, ra = fst raDec,dec = snd raDec})
			  where raDec = getRaDec y m d p
{-toStar [s2,s3,s4,s5] = (Star {name="", constell=s2, ra=(read s3), dec=(read s4), mag=(read s5)})
toStar _ = (Star "tara" "sitara" 10.0 10.0 7.0)-}
isSet::PlanetWithPos->Bool
isSet p = ((ra p) > 0)
--showStar::Star->String
showPlanetPos p = 
	let sep=" " 
	in ((fmap toUpper $ show (planet p))++sep++(show (ra p))++sep++(show $ dec p )++"\n")

raDecToAltAzimuth::Double->Double->PlanetWithPos->PlanetWithPos
raDecToAltAzimuth lat lst p = PlanetWithPos (planet p) (fst (getAltAzimuth (ra p) (dec p) lat lst)) (snd (getAltAzimuth (ra p) (dec p) lat lst))


altAzFileIO year month day hour minute second lat long =  do
        let planetList = fmap (toPlanetWithPos year month day) [Mercury,Venus,Mars,Jupiter,Saturn,Uranus,Neptune,Moon]
        let lst = getLstFromLongDatetime long year month day hour minute second
        let altAzStarList = filter isSet $ fmap (raDecToAltAzimuth lat lst) planetList
        let sunRaDec = getRADEC year month day
        let sunAltAz = getAltAzimuth (fst sunRaDec) (snd sunRaDec) lat lst
        let sunString = if (fst sunAltAz)<0 then [] else ["SUN "++(show $ fst sunAltAz)++" "++(show $ snd sunAltAz)]
        --sequence $ fmap putStrLn $  fmap show planetList
        --sequence $ fmap putStrLn $  fmap show altAzStarList
        writeFile "gui/planet.txt" ""
        sequence $ fmap (appendFile "gui/planet.txt") $ ((fmap showPlanetPos altAzStarList)++sunString)
        
main = do
        [yearStr,monthStr,dayStr,hourStr,minuteStr,secondStr,latStr,longStr] <- getArgs
        let year = read yearStr
        let month = read monthStr
        let day = read dayStr
        let hour = read hourStr
        let minute = read minuteStr
        let second = read secondStr
        let lat = read latStr
        let long = read longStr
        altAzFileIO year month day hour minute second lat long
