Given the coordinates (latitude, longitude) of observer and date and time; we will display the view of the night sky visible from that particular location at that time (locations of major stars and all planets).

To install:
1. Clone this repository
2. Give proper permissions to nightSkySimulator.sh and execute it with the following parameters:
	- year
	- month
	- day
	- hour
	- minute
	- second (in fraction if needed)
	- latitude of the observer
	- longitude of the observer
	- output file path