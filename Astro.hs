module Astro
( getAltAzimuth,
 getLstFromLongDatetime,
 degToRad,
 radToDeg,
 daysFromMillenium,
 trigDeg
)where
import Data.Time
import Data.Fixed

daysFromMillenium year month day = diffDays (fromGregorian year month day) $ fromGregorian 2000 01 01

getSecondsAfterMidnight  hour minute second = fromIntegral (hour*3600+minute*60)+second

getMstFromDayNo year month day hour minute second =
    	let seconds = getSecondsAfterMidnight  hour minute second
	    dayNo = seconds/86400 + (fromIntegral (daysFromMillenium year month day))
	in seconds + 24110.54841 + 8640184.812866 * dayNo/36525.0 + 0.093104 * dayNo*dayNo/(36525.0*36525.0) - 0.0000062 * dayNo*dayNo*dayNo/(36525.0*36525.0*36525.0)

getLstFromLongDatetime longitude year month day hour minute second=
	longitude + (mod' (getMstFromDayNo year month day hour minute second) 86400)/240.0

degToRad x=x*pi/180.0

radToDeg x = x*180/pi

trigDeg f x = (f . degToRad) x
 
getAltAzimuth ra dec lat lst= 
	let ha  = lst - ra
	    haRad = degToRad ha
	    decRad= degToRad dec
	    latRad= degToRad lat
	    alt	  = asin( ((sin decRad) * (sin latRad)) + ( (cos decRad) * (cos latRad) * (cos haRad) ) )
	    azUnsigned = acos ( ( (sin decRad) - ((sin alt) * (sin latRad)) ) / ((cos alt) * (cos latRad)) )
	    az =mod' (signum(sin(haRad)) * (-1.0) * azUnsigned) (2*pi)
	in (alt,az)
