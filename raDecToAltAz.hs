import System.IO
import System.Environment
import Data.Char
import Astro
wordsWhen     :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
                      "" -> []
                      s' -> w : wordsWhen p s''
                            where (w, s'') = break p s'
data Star = Star { name::String, constell::String, ra::Double, dec::Double, mag::Float} deriving (Show)

toStar::[String]->Star
toStar [s1,s2,s3,s4,s5] = (Star {name=s1, constell=s2, ra=(read s3), dec=(read s4), mag=(read s5)})
toStar [s2,s3,s4,s5] = (Star {name=" ", constell=s2, ra=(read s3), dec=(read s4), mag=(read s5)})
toStar _ = (Star " " " " 0.0 0.0 6.0)
isSet::Star->Bool
isSet star = ((ra star) > 0)
showStar::Star->String
showStar s = 
	let sep="@" 
	in (name s)++sep++(constell s)++sep++(show $ mag s)++sep++(show $ ra s)++sep++(show $ dec s)++"\n"

raDecToAltAzimuth::Double->Double->Star->Star
raDecToAltAzimuth lat lst star = Star (name star) (constell star) (fst (getAltAzimuth (ra star) (dec star) lat lst)) (snd (getAltAzimuth (ra star) (dec star) lat lst)) (mag star)
altAzFileIO year month day hour minute second lat long =  do
        inh <- openFile "finalTable.csv" ReadMode
        fstr1 <- hGetContents inh
        let lineList = lines fstr1
        let recordList = fmap (wordsWhen (=='@')) lineList
        let starList = fmap toStar recordList
        let lst = getLstFromLongDatetime long year month day hour minute second
        let altAzStarList = filter isSet $ fmap (raDecToAltAzimuth lat lst) starList
        ---sequence $ fmap putStrLn $  fmap show starList
        writeFile "gui/data.txt" ""
        sequence $ fmap (appendFile "gui/data.txt") $ (fmap showStar altAzStarList)
        
main = do
        [yearStr,monthStr,dayStr,hourStr,minuteStr,secondStr,latStr,longStr] <- getArgs
        let year = read yearStr
        let month = read monthStr
        let day = read dayStr
        let hour = read hourStr
        let minute = read minuteStr
        let second = read secondStr
        let lat = read latStr
        let long = read longStr
        altAzFileIO year month day hour minute second lat long
