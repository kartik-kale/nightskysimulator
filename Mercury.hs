--module Planets where

import Data.Fixed
import qualified Sun as S
import qualified Astro as A

data Planets = Mercury | Venus | Mars | Jupiter | Saturn | Uranus | Neptune deriving (Show)

-------------------------------------------------------------
-- (Long of asc. node)
isN Mercury d = 48.3313 + 3.24587E-5 * d  

--(Inclination)
i Mercury d= 7.0047 + 5.00E-8 * d

--(Argument of perihelion)
w Mercury d= 29.1241 + 1.01444E-5 * d

-- (Semi-major axis)
a Mercury = 0.387098

-- (Eccentricity)
e Mercury d= 0.205635 + 5.59E-10 * d   

isM Mercury d= mod' (168.6562 + 4.0923344368 * d) 360


------------------------------------------------------------

-- Orbital elements of Venus:
------------------------------

isN Venus d= 76.6799 + 2.46590E-5 * d

i Venus d= 3.3946 + 2.75E-8 * d

w Venus d=  54.8910 + 1.38374E-5 * d

a Venus = 0.723330 

e Venus d= 0.006773 - 1.302E-9 * d

isM Venus d =  48.0052 + 1.6021302244 * d


{-
----------------------------------------------------------        


--Orbital elements of Mars:
---------------------------
isN Mars d =  49.5574 + 2.11081E-5 * d

i Mars d = 1.8497 - 1.78E-8 * d

w Mars d = 286.5016 + 2.92961E-5 * d

a Mars= 1.523688

e Mars d = 0.093405 + 2.516E-9 * d

isM Mars d =  18.6021 + 0.5240207766 * d

-------------------------------------------------------------


--Orbital elements of Jupiter:
-----------------------------
isN Jupiter d = 100.4542 + 2.76854E-5 * d

i Jupiter d = 1.3030 - 1.557E-7 * d

w Jupiter d = 273.8777 + 1.64505E-5 * d

a Jupiter d = 5.20256

e Jupiter d = 0.048498 + 4.469E-9 * d

isM Jupiter d =  19.8950 + 0.0830853001 * d



---------------------------------------------------------------


--Orbital elements of Saturn:
-----------------------------

isN Saturn d = 113.6634 + 2.38980E-5 * d

i Saturn d = 2.4886 - 1.081E-7 * d

w Saturn d = 339.3939 + 2.97661E-5 * d

a Saturn = 9.55475

e Saturn d = 0.055546 - 9.499E-9 * d

isM Saturn d = 316.9670 + 0.0334442282 * d


--------------------------------------------------------------


--Orbital elements of Uranus:
----------------------------

isN Uranus d =  74.0005 + 1.3978E-5 * d

i Uranus d = 0.7733 + 1.9E-8 * d

w Uranus d =  96.6612 + 3.0565E-5 * d

a Uranus d = 19.18171 - 1.55E-8 * d 

e Uranus d = 0.047318 + 7.45E-9 * d

isM Uranus d = 142.5905 + 0.011725806 * d


----------------------------------------------------------------

--Orbital elements of Neptune:
-----------------------------

isN Neptune d = 131.7806 + 3.0173E-5 * d

i Neptune d = 1.7700 - 2.55E-7 * d

w Neptune d = 272.8461 - 6.027E-6 * d

a Neptune d = 30.05826 + 3.313E-8 * d

e Neptune d = 0.008606 + 2.15E-9 * d

isM Neptune d = 260.2471 + 0.005995147 * d

---------------------------------------------------------------
-}
