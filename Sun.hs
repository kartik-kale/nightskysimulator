module Sun (getRADEC, r, lonsun, getEcl) where

import Data.Fixed
import Astro 

isN = 0.0

--i :: (Floating a)=> a
i = 0.0

--w ::(Floating a)=> a -> a
w d = 282.9404 + 4.70935E-5 * d

a = 1.000000

--e ::(Floating a, Real a)=> a -> a
e d = 0.016709 - 1.151E-9 * d
      
--isM ::(Real a, Ord a, Floating a)=> a -> a
isM d = mod' (356.0470 + 0.9856002585 * d)  360
  
--getEccentricAnomaly :: (Real a, Floating a) => a -> a  
getEccentricAnomaly d = isM d + (e d*(180/pi)) * sin (degToRad (isM d)) * ( 1.0 + e d* cos (degToRad (isM d)))

--xv :: (Real a, Floating a)=> a -> a
xv d= cos (degToRad $ getEccentricAnomaly d) - e d

yv d = sqrt (1.0 -e d*e d) * sin (degToRad $ getEccentricAnomaly d)

v d= radToDeg (atan2 (yv d) (xv d))

r d= sqrt( xv d*xv d+ yv d*yv d)

--lon :: a -> a
--lon d = lonsun d d
lonsun d= mod' (v d + w d) 360


xs d= r d * cos(degToRad (lonsun d))
ys d= r d * sin(degToRad (lonsun d))

getEcl d= 23.4393-(3.563E-7 * d)

--xe :: (RealFloat a)=> a -> a
xe = xs
ye d= ys d * cos(degToRad (getEcl d))
ze d= ys d * sin(degToRad (getEcl d))

--rA :: (RealFloat a)=> a -> a
rA d = radToDeg $ atan2 (ye d) (xe d)
dec d = radToDeg $  atan2 (ze d) (sqrt(xe d *xe d+ye d *ye d) )

--getRADEC :: (RealFloat a, Int a)=> a -> a -> a -> a
getRADEC y m d = (rA days, dec days)
                     where        
                       days=fromIntegral $ daysFromMillenium y m d
