{-# LANGUAGE NoMonomorphismRestriction #-}
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine
 -- or:
 -- import Diagrams.Backend.xxx.CmdLine
 -- where xxx is the backend you would like to use.

			
example :: Diagram B
example2 :: Diagram B
-- example3 :: Diagram B

circlecreator :: (Double, Double, Double) -> Diagram B
circlecreator (a,b,c) = (circle a # fc white # lw none # translate (r2 (b, c)))

skycreator :: [(Double, Double, Double)] -> Diagram B
skycreator x = foldr (atop) (baselineText ("GAUREE") # fontSizeL 5 # fc white <> circle 100 # fc black # lw none) (map circlecreator x)
--skycreator x = foldl (atop) (circle 100 # fc black # lw none) (map circlecreator x)
  
-- example3 :: Diagram B
example = atop (circle 10 # fc black # lw none # translate (r2 (10, 0)))(circle 10 # fc black # lw none # translate (r2 (-10, 0)))
example2 = (atop (circle 1 # fc white # lw none # translate (r2 (-40, 10))) example) # showOrigin
-- example2 = (fc black . lw thick $ circle 10) `atop` (translate (r2 (0.5, 0.3).fc white . lw thick $ circle 5))
                  
--main = mainWith example
main = (mainWith (skycreator [(1,0,0),(1,20,20), (1,-20,20), (1,-20,-20), (1,20,-20)]))
