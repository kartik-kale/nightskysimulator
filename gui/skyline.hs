{-# LANGUAGE NoMonomorphismRestriction #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine


import System.IO
import Data.Char


circlecreator :: ([Char], [Char], Double, Double, Double) -> Diagram B
circlecreator (a,b,c,d,e) = (baselineText (a) # fontSizeL 5 # fc white # translate (r2 (d, e))<> circle c # fc white # lw none # translate (r2 (d, e)))

skycreator :: [([Char], [Char], Double, Double, Double)] -> Diagram B
skycreator x = foldr (atop) (baselineText (".") # fontSizeL 5 # fc white <> circle 500 # fc black # lw none) (map circlecreator x)

fun :: String -> Double
fun a = read $ a

takeFive :: [a] -> [a]
takeFive [] = []
takeFive (a:b:c:d:e:xs) = [a,b,c,d,e]	

makeDataSet :: [[Char]] -> [([Char], [Char], Double, Double, Double)]
makeDataSet [] = []
makeDataSet (a:b:c:d:e:xs) = (a,b,fun c, fun d, fun e):makeDataSet xs


main = do
       inh <- openFile "data.txt" ReadMode 
       fstr1 <- hGetContents inh
       let startuple = makeDataSet (words fstr1) 
       mainWith (skycreator startuple) 

