{-# LANGUAGE NoMonomorphismRestriction #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine


import System.IO
import Data.Char


--Creates Circle from Coords
circlecreator :: ([Char], [Char], Double, Double, Double) -> Diagram B
circlecreator (a,b,c,d,e) = (baselineText (a) # fontSizeL 5 # fc white # translate (r2 (d, e))<> circle c # fc white # lw none # translate (r2 (d, e)))

--Superimposes dots to form NightSky
skycreator :: [([Char], [Char], Double, Double, Double)] -> Diagram B
skycreator x = foldr (atop) (baselineText (".") # fontSizeL 5 # fc white <> circle 250 # fc black # lw none) (map circlecreator x)

--Gives constellName from 5 tuple
constell :: ([Char], [Char], Double, Double, Double) -> String
constell (_,a,_,_,_) = a

--Adds Star 5 Tuple
addConstell :: ([Char],[Char],Double,Double,Double) -> ([Char], [Char], Double, Double, Double) -> ([Char], [Char], Double, Double, Double) 
addConstell (a,b,c,d,e) (p,q,r,s,t) = ("",b,c+r,d+s,e+t)

--Divides Star 5 Tuple by Number
divConstell :: ([Char],[Char],Double,Double,Double) -> Int -> ([Char], [Char], Double, Double, Double) 
divConstell (a,b,c,d,e) n = (a,b,c/(fromIntegral n),d/(fromIntegral n),e/(fromIntegral n))


--Gives average location of a Constellation from a list
avgConstell :: [([Char], [Char], Double, Double, Double)] -> ([Char], [Char], Double, Double, Double)
avgConstell [] = ("","",0,0,0)
avgConstell x = divConstell (foldl addConstell ("",(constell (head x)),0,0,0) x) (length x)

-- Converts a String to Double
fun :: String -> Double
fun a = read $ a

--Filters the starlist by Constellation Name
constellFilter :: [([Char], [Char], Double, Double, Double)] -> String -> [([Char], [Char], Double, Double, Double)]
constellFilter a [] = a
constellFilter [] _ = []
constellFilter (x:xs) b  
    | ((constell x) == b) = x:(constellFilter xs b)
    | otherwise = (constellFilter xs b) 
   

--Makes a Diagram from a Constell 
constellCreator :: ([Char], [Char], Double, Double, Double) -> Diagram B
constellCreator (a,b,c,d,e) = (text (b) # fontSizeL 5 # fc white # translate (r2 (d, e))<> circle 0.5 # fc white # lw none # translate (r2 (d, e)))

--Makes a tuple from the first 5 values in a list
takeFive :: [a] -> [a]
takeFive [] = []
takeFive (a:b:c:d:e:xs) = [a,b,c,d,e]	

--Converts words to a List of 5 Tuples
makeDataSet :: [[Char]] -> [([Char], [Char], Double, Double, Double)]
makeDataSet [] = []
makeDataSet (a:b:c:d:e:xs) = (a,b,fun c, fun d, fun e):makeDataSet xs


main = do
       inh <- openFile "data.txt" ReadMode 
       fstr1 <- hGetContents inh
       let startuple = makeDataSet (words fstr1) 
       consName <- getLine 
       let startuple2 = (constellFilter startuple consName)
       putStr (show startuple2) 
       putStr (show (avgConstell startuple2))      
       mainWith (atop (constellCreator (avgConstell startuple2)) (skycreator startuple2))

