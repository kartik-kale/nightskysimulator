import Data.Char
import System.IO

-- Takes a string of alphabets and converts lower case characters into respective Upper Case characters
toTotalUpper :: [Char] -> [Char]
toTotalUpper [] = []
toTotalUpper (x:xs) = (toUpper x):(toTotalUpper xs)

-- Takes a string of alphabets and removes all non Non Alphabetic Characters. White spaces are preserved.
removePunc :: [Char] -> [Char]
removePunc [] = []
removePunc (x:xs) = if((isAlpha x) == True || (isSpace x) == True) then x:(removePunc xs) else (removePunc xs)

-- Takes a list and a value as arguments and return frequency of the value in the list.
count :: Eq a => a -> [a] -> Int
count _ [] = 0
count a (x:xs) = if (a==x) then (1+(count a xs)) else (count a xs)

-- Takes a string of alphabets and generates Tuples which contains words and respective count.
generateCount :: Eq a => [a] -> [(a,Int)]
generateCount [] = []
generateCount (x:xs) = (x, (count x (x:xs))):generateCount (filter (/= x) xs)

-- Prints the word and respective count, which is stored in Tuple form
showTuple :: Show a => ([Char], a) -> IO ()
showTuple (a,b) = putStrLn("Word " ++ a ++ " has count " ++ (show b))

-- Applies showTuple on a list of Tuples
showWords :: Show a => [([Char], a)] -> IO ()
showWords [] = putStrLn ""
showWords [x] = showTuple x
showWords (x:xs) = do 
                   showTuple x
		   showWords xs

-- Prints a list of Words.
showAllWords :: [String] -> IO ()
showAllWords [] = putStrLn "\n"
showAllWords (x:xs) = do
			putStrLn x
			showAllWords xs

-- Screens the elements of a list which are NOT elements of another list
screen :: Eq a => [a] -> [a] -> [a]
screen [] _ = []
screen (x:xs) a = if((elem x a) == False) then x:(screen xs a) else (screen xs a)



main :: IO ()
main = do 
       inh <- openFile "TestText.txt" ReadMode
       mainloop inh 
       hClose inh


mainloop :: Handle -> IO ()
mainloop inh = do 
	            
                    let fstr = removePunc (hGetContents inh)
       		    let fstr2 = words fstr
		    let y = length fstr2
		    putStrLn ("Total Number of Words = " ++ (show y) ++ "\n")
                    putStrLn ("The List of Words is as follows :" ++ "\n")
		    let fstr4 = map toTotalUpper fstr2	
		    showAllWords fstr4
                    putStrLn ("The Frequency of Non Noise Words is as follows :" ++ "\n")
		    let fstr3 = generateCount fstr4
                    showWords fstr3
               





