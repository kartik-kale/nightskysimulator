--ALL GALAXIES ON ONE Go
--FILTER

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine


import System.IO
import Data.Char

wordsWhen     :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
                      "" -> []
                      s' -> w : wordsWhen p s''
                            where (w, s'') = break p s'
--Reject a certain element
reject :: Eq a => [a] -> a -> [a]
reject [] a = []
reject (x:xs) a   
	| (a == x) = reject xs a
    	| otherwise = x:(reject xs a) 

circleRad = 125
radFromMag mag = ((7-mag)^2)/40

--Remove Repititions is a list
unique [] = []
unique (x:xs) = x:(unique (reject xs x))	

--Generates MapRadius from Angle of Elevation
altToRadius :: Double -> Double
altToRadius x = if (x > 0) then (pi/2-x)*2*circleRad/pi else x

--Generates MapAngle from Azimuthal Angle
mapAngle :: Double -> Double
mapAngle x = x + (pi/2)


--Converts Data into Plottable Form
procTuple :: ([Char],[Char],Double,Double,Double) -> ([Char], [Char], Double, Double, Double) 
procTuple (a,b,c,d,e)  = (a,b,c,(altToRadius d)*(cos(mapAngle e)),(altToRadius d)*(sin(mapAngle e)))


--Creates Circle from Coords
circlecreator :: ([Char], [Char], Double, Double, Double) -> Diagram B
circlecreator (a,b,c,d,e) 

		| (c > 1.5) = (circle (radFromMag c) # fc white # lw none # translate (r2 (d, e)))		
		| otherwise = (baselineText (a) # fontSizeL 3 # fc white # translate (r2 (d, e))<> circle (radFromMag c) # fc white # lw none # translate (r2 (d, e)))

--Superimposes dots to form NightSky
skycreator :: [([Char], [Char], Double, Double, Double)] -> Diagram B
skycreator x = foldr (atop) (baselineText ("") # fontSizeL 5 # fc white <> circle circleRad # fc black # lw none) (map circlecreator x)

--Gives constellName from 5 tuple
constell :: ([Char], [Char], Double, Double, Double) -> String
constell (_,a,_,_,_) = a

--Makes list of Constellations
constList :: [([Char], [Char], Double, Double, Double)] -> [[Char]]
constList [] = []
constList (x:xs) = (constell x):(constList xs)

--Adds Star 5 Tuple
addConstell :: ([Char],[Char],Double,Double,Double) -> ([Char], [Char], Double, Double, Double) -> ([Char], [Char], Double, Double, Double) 
addConstell (a,b,c,d,e) (p,q,r,s,t) = ("",b,c+r,d+s,e+t)

--Divides Star 5 Tuple by Number
divConstell :: ([Char],[Char],Double,Double,Double) -> Int -> ([Char], [Char], Double, Double, Double) 
div _ 0 = ("","",0,0,0)
divConstell (a,b,c,d,e) n = (a,b,c/(fromIntegral n),d/(fromIntegral n),e/(fromIntegral n))


--Gives average location of a Constellation from a list
avgConstell :: [([Char], [Char], Double, Double, Double)] -> ([Char], [Char], Double, Double, Double)
avgConstell x = divConstell (foldl addConstell ("",(constell (head x)),0,0,0) x) (length x)

-- Converts a String to Double
fun :: String -> Double
fun a = read $ a

--Filters the starlist by Constellation Name
constellFilter :: [([Char], [Char], Double, Double, Double)] -> String -> [([Char], [Char], Double, Double, Double)]
constellFilter a [] = a
constellFilter [] _ = []
constellFilter (x:xs) b  
    | ((constell x) == b) = x:(constellFilter xs b)
    | otherwise = (constellFilter xs b) 
   

--Makes a Diagram from a Constell 
constellCreator :: ([Char], [Char], Double, Double, Double) -> Diagram B
constellCreator (a,b,c,d,e) = (text (b) # fontSizeL 4 # fc blue # translate (r2 (d, e))<> circle 0.5 # fc white # lw none # translate (r2 (d, e)))

--Makes a tuple from the first 5 values in a list
takeFive :: [a] -> [a]
takeFive [] = []
takeFive (a:b:c:d:e:xs) = [a,b,c,d,e]	

--Extracts the first three values from a list
takeThree :: [[Char]] -> ([Char], Double, Double)
takeThree [] = ("",0,0)
takeThree (a:b:c:xs) = (a, (altToRadius (fun b))*(cos(mapAngle (fun c))),(altToRadius (fun b))*(sin(mapAngle (fun c))))

--Converts words to a List of 5 Tuples
makeDataSet :: [[Char]] -> [([Char], [Char], Double, Double, Double)]
makeDataSet [] = []
makeDataSet [a] = []
makeDataSet [a,b] = []
makeDataSet [a,b,c] = []
makeDataSet [a,b,c,d] = []
makeDataSet (a:b:c:d:e:xs) = (a,b,fun c, fun d, fun e):makeDataSet xs

--Makes a list of planet Tuples
makePlanet :: [[Char]] -> [([Char], Double, Double)]
makePlanet [] = []
makePlanet x = (takeThree x):(makePlanet (tail (tail (tail x))))

--Creates PlanetImages from Tuples
createPlanet :: ([Char], Double, Double) -> Diagram B
createPlanet (a,b,c) 
		| (a == "MERCURY") = (baselineText (a) # fontSizeL 3 # fc orange # translate (r2 (b, c))<> circle (radFromMag 0) # fc orange # lw none # translate (r2 (b, c)))	
		| (a == "VENUS") = (baselineText (a) # fontSizeL 3 # fc lightblue # translate (r2 (b, c))<> circle (radFromMag (-2)) # fc lightblue # lw none #  translate (r2 (b, c)))	
		| (a == "MARS") = (baselineText (a) # fontSizeL 3 # fc red # translate (r2 (b, c))<> circle (radFromMag (-1.5)) # fc red # lw none # translate (r2 (b, c)))	
		| (a == "JUPITER") = (baselineText (a) # fontSizeL 3 # fc yellow # translate (r2 (b, c))<> circle (radFromMag (-1.5)) # fc yellow # lw none # translate (r2 (b, c)))	
		| (a == "SATURN") = (baselineText (a) # fontSizeL 3 # fc aquamarine # translate (r2 (b, c))<> circle (radFromMag (0)) # fc aquamarine # lw none # translate (r2 (b, c)))	
		| (a == "URANUS") = (baselineText (a) # fontSizeL 3 # fc snow # translate (r2 (b, c))<> circle (radFromMag (2)) # fc snow # lw none # translate (r2 (b, c)))	
		| (a == "NEPTUNE") = (baselineText (a) # fontSizeL 3 # fc skyblue # translate (r2 (b, c))<> circle (radFromMag (2)) # fc skyblue # lw none # translate (r2 (b, c)))
		| (a == "MOON") = (baselineText (a) # fontSizeL 3 # fc grey # translate (r2 (b, c))<> circle (radFromMag (-2.5)) # fc grey # lw none # translate (r2 (b, c)))
		| (a == "SUN") = (baselineText (a) # fontSizeL 3 # fc gold # translate (r2 (b, c))<> circle (radFromMag (-3)) # fc gold # lw none # translate (r2 (b, c)))
		| otherwise = (baselineText (a) # fontSizeL 3 # fc green # translate (r2 (b, c))<> circle (radFromMag (2)) # fc green # lw none # translate (r2 (b, c)))


main = do
	inh <- openFile "data.txt" ReadMode
        fstr1 <- hGetContents inh
        let lineList = lines fstr1
  	let recordList = fmap (wordsWhen (=='@')) lineList
	let recordList2 = foldl (++) [] recordList         
	let startuple = makeDataSet recordList2 
        let listOfCons = (constList startuple)
        let listOfCons2 = (unique listOfCons)
	let list3 = map (constellFilter startuple) listOfCons2
        let list4 = map avgConstell list3
	let list5 = map circlecreator (map procTuple startuple)
	let list6 = map constellCreator (map procTuple list4)
	--putStr (show (map procTuple list4)) 
        let list7 = list5 ++ list6
        inh2 <- openFile "planet.txt" ReadMode 
        fstr2 <- hGetContents inh2
        let planettuple = makePlanet (words fstr2) 
        let list8 = map createPlanet planettuple
        let list9 = list8 ++ list7
        mainWith (foldr (atop) (baselineText ("") # fontSizeL 5 # fc white <> circle (circleRad*1.05) # fc black # lw none) (list9))

