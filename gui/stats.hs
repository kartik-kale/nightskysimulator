{-# LANGUAGE NoMonomorphismRestriction #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine


import System.IO
import Data.Char


altToRadius :: Double -> Double
altToRadius x = ((3.14/2 - x)/3.14)*500

mapAngle :: Double -> Double
mapAngle x = x + (3.14/2)
